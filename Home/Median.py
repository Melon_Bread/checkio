def checkio(data):
    data.sort()
    half = len(data)//2
    if len(data) % 2 != 0:
        return half
    else:
        median = (data[half] + data[half - 1]) / 2
        return median

if __name__ == '__main__':
    checkio([1, 2, 3, 4, 5])
    checkio([3, 1, 2, 5, 3])
    checkio([1, 300, 2, 200, 1])
    checkio([3, 6, 20, 99, 10, 15])
    print("Start the long test")
    checkio(list(range(1000000)))
    print("The local tests are done.")
