def checkio(data):
    dataNew = []
    for x in data:
        if data.count(x) > 1:
            dataNew.append(x)
    return dataNew

if __name__ == "__main__":
    checkio([1, 2, 3, 1, 3])
    checkio([1, 2, 3, 4, 5])
    checkio([5, 5, 5, 5, 5])
    checkio([10, 9, 10, 10, 9, 8])
