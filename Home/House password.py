def checkio(data):
    if len(data) <= 9:
        return False
    else:
        lower = False
        upper = False
        digit = False
        for x in data[:]:
            if str.islower(x):
                lower = True
            elif str.isupper(x):
                upper = True
            elif str.isdigit(x):
                digit = True
        if lower == True and upper == True and digit == True:
            return True
        else:
            return False

if __name__ == '__main__':
    checkio('A1213pokl')
    checkio('bAse730onE4')
    checkio('asasasasasasasaas')
    checkio('QWERTYqwerty')
    checkio('123456123456')
    checkio('QwErTy911poqqqq')
