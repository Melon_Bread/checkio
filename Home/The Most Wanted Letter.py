def checkio(text):
    text = sorted(str.lower(text))
    mostCount = 0
    mostChar = ''
    for x in text[:]:
        if str.isalpha(x):
            if text.count(x) > mostCount:
                mostCount = text.count(x)
                mostChar = x

    return print(mostChar)

if __name__ == '__main__':
    checkio("Hello World!")
    checkio("How do you do?")
    checkio("One")
    checkio("Oops!")
    checkio("AAaooo!!!!")
    checkio("abe")
    print("Start the long test")
    checkio("a" * 9000 + "b" * 1000)
    print("The local tests are done.")
