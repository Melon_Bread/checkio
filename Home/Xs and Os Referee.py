def checkio(game):
    #Check Rows
    for x in game:
        if x[0] == x[1] == x[2] and x[0] != '.':
            return x[0]
    #Check Colums
    for x in range(len(game)):
        match = 0
        for y in range(len(game[x])):
            if game[y][x] == game[x][x] and game[x][x] !='.':
                match+=1
                if match == 3:
                    return game[y][x]
    #Check Diags
    if game[1][1] != '.':
        if game[0][0] == game[1][1] == game[2][2] or game[0][2] == game[1][1] == game[2][0]:
            return game[1][1]
    #Returns Draw
    return "D"

if __name__ == '__main__':
    checkio([
        "X.O",
        "XX.",
        "XOO"])
    checkio([
        "OO.",
        "XOX",
        "XOX"])
    checkio([
        "OOX",
        "XXO",
        "OXX"])
    checkio([
        "O.X",
        "XX.",
        "XOO"])
