def checkio(array):
    if array == []:
        return print(0)
    else:
        return print(sum(array[::2]) * array[-1])

if __name__ == '__main__':
    checkio([0, 1, 2, 3, 4, 5])
    checkio([1, 3, 5])
    checkio([6])
    checkio([])
