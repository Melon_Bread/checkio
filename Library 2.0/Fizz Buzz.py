def checkio(number):
    if number%3 == 0 and number%5 == 0:
        return ("Fizz Buzz")
    elif number%3 == 0:
        return ("Fizz")
    elif number%5 == 0:
        return ("Buzz")
    else:
        return str(number)

if __name__ == '__main__':
    checkio(15)
    checkio(6)
    checkio(5)
    checkio(7)
